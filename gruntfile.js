module.exports = function(grunt) {

  // Load all tasks
  require('load-grunt-tasks')(grunt);
  require('time-grunt')(grunt);

  var mozjpeg = require('imagemin-mozjpeg');

  var htmlFileList = [
    'index.html'
  ];

  var cssFileList = [
    'bower_components/normalize.css/normalize.css'
  ];

  var jsFileList = [
    'bower_components/angular/angular.min.js',
    'bower_components/angular-route/angular-route.min.js',
    'bower_components/angular-sanitize/angular-sanitize.min.js',
    'bower_components/angular-touch/angular-touch.min.js',
    'bower_components/firebase/firebase.js',
    'bower_components/angularfire/dist/angularfire.min.js',
    'bower_components/jquery/dist/jquery.js'
  ];
  
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    concat: { 
        options: {
          separator: ';'
        },  
        build: {
          files: {
            'static/main.min.js': ['assets/scripts/**/*.js'],
            'static/vendors.min.js': [jsFileList]
          }
        }
    },
    uglify: {
        build: {
          files: {
            '../dist/assets/scripts/main.min.js': 'assets/scripts/**/*.js',
            '../dist/assets/scripts/vendors.min.js': [jsFileList]
          }
        }
    },
    imagemin: {
      dynamic: {
        options: {
          optimizationLevel: 4,
          svgoPlugins: [{ removeViewBox: false }],
          progressive: true,
          use: [mozjpeg({quality: 80})]
        },
        files: [{
          expand: true, // Enable dynamic expansion
          cwd: 'assets/images/', // Src matches are relative to this path
          src: ['**/*.{png,jpg,gif,svg}'], // Actual patterns to match
          dest: '../dist/assets/images' // Destination path prefix
        }]
      }
    }, 
    sass: {
      dev: {
        options: {
          style: 'expanded',
          sourcemap: 'auto'
        },
        files: {
          'assets/styles/style.css': 'assets/styles/style.scss'
        }
      },
      build: {
        options: {
          style: 'compressed'
        },
        files: {
          '../dist/assets/styles/style.min.css': 'assets/styles/style.scss'
        }
      }
    },   
    autoprefixer: {
      options: {
        browsers: ['last 2 versions', '> 1%', 'ff > 20', 'ie 8', 'ie 9', 'android 2.3', 'android 4', 'opera 12']
      },
      dev: {
        options: {
          map: {
            prev: 'assets/styles/'
          }
        },
        src: 'assets/styles/style.css'
      },
      build: {
        src: 'assets/styles/style.css'
      }
    },
    processhtml: {
      build: {
        files: {
          '../dist/index.html': ['index.html']
        }
      }
    },
    uncss: {
      options: {
        compress: true,
        report: 'min',
      },
      build: {
        files: {
          '../dist/assets/styles/style.min.css': [htmlFileList]
        }
      }
    },
    copy:{
      build: {
        files: [
          {
            expand: true, 
            cwd: 'assets/fonts/', 
            src: ['**'], 
            dest: '../dist/assets/fonts/', 
            flatten: true, 
            filter: 'isFile'
          },
          {
            expand: true, 
            cwd: '', 
            src: 'favicon.ico', 
            dest: '../dist/', 
            flatten: true, 
            filter: 'isFile'
          }
        ]
      }
    },
    'ftp-deploy': {
      deploy: {
        auth: {
          authPath: '../.ftppass',
          host: 'HOSTHERE',
          port: 21,
          authKey: 'key1',
        },
        src: '../dist/',
        dest: 'ftpdeploytest/',
        exclusions: [
          '.DS_Store',
          '.gitignore',
          '.ftppass'
        ]
      }
    },
    connect: {
      all: {
        options:{
          port: 9000,
          hostname: "0.0.0.0",
          keepalive: false
        }
      }
    },
    open: {
      all: {
        path: 'http://localhost:<%= connect.all.options.port%>'
      }
    },
    browserSync: {
      dev: {
        bsFiles: {
          src : [
            'assets/styles/style.css',
            '*.html'
          ]
        },
        options: {
          watchTask: true,
          server: '.'
        }
      }
    },
    watch: {
      js: {
          files: ['assets/scripts/**/*.js'],
          tasks: ['concat'],
          options: {
              spawn: false,
          },
      },
      css: {
        files: ['assets/styles/**/*.scss'],
        tasks: ['sass:dev', 'autoprefixer:dev'],
      }
    }   
  });

  grunt.registerTask('default', [
      'dev'
  ]);

  grunt.registerTask('dev', [
      'sass:dev',
      'autoprefixer:dev',
      'concat'
  ]);

  grunt.registerTask('build', [
      'newer:sass:build',
      'newer:autoprefixer:build',
      'newer:concat',
      'newer:uglify',
      'newer:imagemin',
      'processhtml',
      'newer:uncss',
      'newer:copy:build'
  ]);

  grunt.registerTask('live', [
      'dev',
      'browserSync',
      'watch'
  ]);

  grunt.registerTask('serve',[
      'dev',
      'open',
      'connect',
      'watch'
  ]);

  grunt.registerTask('deploy', [
      'build',
      'ftp-deploy'
  ]);

};