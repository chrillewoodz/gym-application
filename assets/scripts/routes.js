// Create secure routes
//
// Call via resolve
core.run(['$rootScope', '$location', function($rootScope, $location) {
  $rootScope.$on('$routeChangeError', function(event, next, previous, error) {
    if (error === 'AUTH_REQUIRED') {
        $location.path('/');
    }
  });
}]);

core.config(['$routeProvider', function($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'views/pages/landing.html',
      resolve: {
        'isAuth': ['fbRefs', function(fbRefs) {
          return fbRefs.getAuthObj().$waitForAuth();
        }]
      }
    })
    .when('/home', {
      templateUrl: 'views/pages/home.html',
      resolve: {
        'isAuth': ['fbRefs', function(fbRefs) {
          return fbRefs.getAuthObj().$requireAuth();
        }]
      }
    })
    .when('/programs', {
      templateUrl: 'views/pages/programs.html',
      resolve: {
        'isAuth': ['fbRefs', function(fbRefs) {
          return fbRefs.getAuthObj().$requireAuth();
        }]
      }
    })
    .when('/programs/:key', {
      templateUrl: 'views/pages/single-program.html',
      resolve: {
        'isAuth': ['fbRefs', function(fbRefs) {
          return fbRefs.getAuthObj().$requireAuth();
        }],
        'programData': ['$route', 'fbRefs', '$firebaseAuth', '$location', function($route, fbRefs, $firebaseAuth, $location) {
          
          // Get our object from $routeParams
          var recordKey = $route.current.params.key;

          // Get unique user id
          var uid = $firebaseAuth(fbRefs.getReference()).$getAuth().uid;

          // Define our path
          var path = uid + '/programs/' + recordKey;

          // Fetch the program from Firebase
          return fbRefs.getSyncedObj(path).$loaded()
            .then(function(res) {
              return res;
            })
            .catch(function(err) {
              $location.path('/error');
          });
        }]
      }
    })
    .when('/diet', {
      templateUrl: 'views/pages/diet.html',
      resolve: {
        'isAuth': ['fbRefs', function(fbRefs) {
          return fbRefs.getAuthObj().$requireAuth();
        }]
      }
    })
    .when('/body', {
      templateUrl: 'views/pages/body.html',
      resolve: {
        'isAuth': ['fbRefs', function(fbRefs) {
          return fbRefs.getAuthObj().$requireAuth();
        }]
      }
    })
    .when('/login', {
      templateUrl: 'views/pages/login.html'
    })
    .when('/register', {
      templateUrl: 'views/pages/register.html'
    })
    .when('/error', {
      templateUrl: 'views/error.html'
    })
    .otherwise({redirectTo: '/'});
}])