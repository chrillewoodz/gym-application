body.controller('BodyCtrl', ['$scope', '$http', function($scope, $http) {
  
  var getBody = $http.get('assets/scripts/modules/body/body.json');
  
  getBody
    .then(function(res) {
      $scope.body = res.data.body;
    })
    .catch(function(err) {
      console.log(err);
  });
  
}]);



