crud.factory('fbCrud', ['$firebase', 'fbRefs', function($firebase, fbRefs) {

  return {
    push: function(path, obj) {
      return fbRefs.getSyncedArray(path).$add(obj);
    },
    set: function(path, obj) {
      return fbRefs.getSyncedObj(path).$set(obj);
    },
    save: function(path, index) {
      return fbRefs.getSyncedArray(path).$save(index);
    },
    remove: function(path, index) {
      return fbRefs.getSyncedObj(path).$remove(index);
    }
  };
}]);
