programs.controller('ProgramCtrl', ['$scope', '$firebaseAuth', 'fbRefs', 'fbCrud', function($scope, $firebaseAuth, fbRefs, fbCrud) {
  
  // Action states, if changing crud
  // functionality for edit and delete
  // add these here if required
  $scope.actions = {
    add: false
  };
  
  // Get the unique id for the user
  var uid = $firebaseAuth(fbRefs.getReference()).$getAuth().uid;
  
  // Specify the path we will be working with
  var path = uid + '/programs';
  
  // Get all programs
  var programs = fbRefs.getSyncedArray(path);
  
  // Create a three way data binding
  // for our programs
  programs.$watch(function(event) {

    // Fetch the programs every time it's updated
    programs = fbRefs.getSyncedArray(path);

    // When the data has finished loading
    // update the programs list
    programs.$loaded()
      .then(function(res) {
        $scope.programs = res;
      })
      .catch(function(err) {
        $scope.addError = 'There was an error with the request';
    });
  });

  // Add a new program to firebase
  $scope.addProgram = function(program) {

    fbCrud.push(path, program)
      .then(function(ref) {
      
        // Get the key for the item
        // that was just inserted
        var key = ref.key();

        // And assign it to the program
        // object for later use
        program.key = key;
      })
      .catch(function(err) {
        $scope.addError = 'Unable to add program';
    });
  };

  // Add records to be removed
  var records = [];

  $scope.addKey = function(key) {
    records.push(key);
  };

  // Delete the records using 
  // the keys specified above
  $scope.deleteRecords = function() {

    var objPath;

    records.map(function(val, i, arr) {

      // Set the path for the program
      // each time we loop
      objPath = path + '/' + arr[i];

      fbCrud.remove(objPath, arr[i])
        .then(function() {
          arr.splice(i, 1);
        })
        .catch(function(err) {
          $scope.delError = 'Failed to delete one or more program';
      });
    });
  };
  
  // Check if the 'check all' box is checked or not
  $scope.$watch('checkAll', function() {

    if ($scope.checkAll) {

      // Make sure we only run map
      // if there are any programs added
      if ($scope.programs) {
        $scope.programs.map(function(item) {
          records.push(item.key);
          item.checked = true;
        });
      }
    }
    else {

      // Make sure we only run map
      // if there are any programs added
      if ($scope.programs) {
        $scope.programs.map(function(item, i) {
          records.splice(i, 1);
          item.checked = false;
        });
      }
    }
  });
  
}]);