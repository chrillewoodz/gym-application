singleProgram.controller('SingleProgramCtrl', ['$scope', '$firebaseAuth', '$routeParams', 'fbRefs', '$route', 'fbCrud', function($scope, $firebaseAuth, $routeParams, fbRefs, $route, fbCrud) {
  
  // Action states, if changing crud
  // functionality for edit and delete
  // add these here if required
  $scope.actions = {
    add: false
  };
  
  // Fetch the program from 
  // our routing's resolve
  $scope.program = $route.current.locals.programData;
  
  // Grab program key from route
  var key = $routeParams.key;
  
  // Get unique user id
  var uid = $firebaseAuth(fbRefs.getReference()).$getAuth().uid;

  // Define our path
  var path = uid + '/programs/' + key + '/exercises';
  
  // Get all exercises
  var exercises = fbRefs.getSyncedArray(path);
  
  // Create a three way data binding
  // for our exercises
  exercises.$watch(function(event) {

    // Fetch the programs every time it's updated
    exercises = fbRefs.getSyncedArray(path);

    // When the data has finished loading
    // update the programs list
    exercises.$loaded()
      .then(function(res) {
        $scope.exercises = res;
      })
      .catch(function(err) {
        $scope.addError = 'There was an error with the request';
    });
  });

  // Add program exercise
  $scope.addExercise = function(exercise) {
    
    fbCrud.push(path, exercise)
      .then(function(ref) {
      
        // Get the key for the item
        // that was just inserted
        var key = ref.key();
      
        // And assign it to the program
        // object for later use
        exercise.key = key;
      })
      .catch(function(err) {
        $scope.addError = 'Unable to add exercise';
    });
  };
  
  $scope.updateExercise = function(exercise) {
    
    // Get the reference key
    var key = exercise.key;
    
    // Save changes to firebase,
    // update status is used in the html
    // to set correct class and status message
    fbCrud.save(path, key)
      .then(function(ref) {
        exercise.updateStatus = true;
      })
      .catch(function(err) {
        exercise.updateStatus = false;
    });
  };
  
  // Add exercises to be removed
  var records = [];

  $scope.addKey = function(key) {
    records.push(key);
  };

  // Delete the records using 
  // the keys specified above
  $scope.deleteExercises = function() {

    var objPath;

    records.map(function(val, i, arr) {

      // Set the path for the exercise
      // each time we loop
      objPath = path + '/' + arr[i];

      fbCrud.remove(objPath, arr[i])
        .then(function() {
          arr.splice(i, 1);
        })
        .catch(function(err) {
          $scope.delError = 'Failed to delete one or more exercises';
      });
    });
  }
  
  // Check if the 'check all' box is checked or not
  $scope.$watch('checkAll', function() {

    if ($scope.checkAll) {

      // Make sure we only run map
      // if there are any programs added
      if ($scope.exercises) {
        $scope.exercises.map(function(item) {
          records.push(item.key);
          item.checked = true;
        });
      }
    }
    else {

      // Make sure we only run map
      // if there are any programs added
      if ($scope.exercises) {
        $scope.exercises.map(function(item, i) {
          records.splice(i, 1);
          item.checked = false;
        });
      }
    }
  });
  
}]);