nav.directive('xAnimation', [function() {
  
  return {
    restrict: 'A',
    link: function($scope, element, attrs) {
      
      element.on('click', function() {

        if (element.hasClass('x')) {
          element.removeClass('x');
        }
        else {
          element.addClass('x');
        }
      });
    }
  }
}]);