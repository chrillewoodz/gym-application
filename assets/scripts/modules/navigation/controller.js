nav.controller('NavCtrl', ['$scope', function($scope) {
  
  $scope.menu = [
    {label: 'Home',        path: '/home'},
    {label: 'My programs', path: '/programs'},
    {label: 'Workschemas', path: '/workschemas'},
    {label: 'Diet',        path: '/diet'},
    {label: 'Body',        path: '/body'},
    {label: 'Development', path: '/development'},
    {label: 'Settings',    path: '/settings'}
  ]
  
  $scope.navBoxes = [
    {label: 'My programs', path: '/programs'},
    {label: 'Workschemas', path: '/workschemas'},
    {label: 'Diet',        path: '/diet'},
    {label: 'Body',        path: '/body'},
    {label: 'Development', path: '/development'}
  ]
}]);