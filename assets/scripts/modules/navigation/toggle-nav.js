nav.directive('toggleNav', [function() {
  
  return {
    restrict: 'A',
    link: function($scope, element, attrs) {
      
      element.on('click', function() {

        $('#site-nav').slideToggle(200);
      });
    }
  }
}]);