auth.factory('users', ['$firebase', '$location', 'fbRefs', function($firebase, $location, fbRefs) {
  return {
    createNewUser: function(newUser) {
      
      var self = this;

      return fbRefs.getAuthObj().$createUser({
        email: newUser.email,
        password: newUser.password
      });
    },
    authUser: function(existingUser) {

      return fbRefs.getAuthObj().$authWithPassword({
        email: existingUser.email,
        password: existingUser.password
      });
    }
  };
}]);