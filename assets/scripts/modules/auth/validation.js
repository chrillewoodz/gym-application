auth.service('validation', ['$q', function($q) {
  return {
    validateSignup: function(newUser) {

      var q = $q.defer();

      for (var info in newUser) {
        if (newUser[info] === '') {
          q.reject('All fields must be filled in');
        }
      }

      if (newUser.email === undefined) {
        q.reject('Invalid email format');
      }
      else if (newUser.password.length < 8) {
        q.reject('The password is too short');
      }
      else if (newUser.password != newUser.confirmedPass) {
        q.reject('The passwords do not match');
      }

      q.resolve(true);

      return q.promise;
    },
    validateSignin: function(existingUser) {

      var q = $q.defer();

      if (existingUser.email === '' || existingUser.password === '') {
        q.reject('You must fill in both fields');
      }
      else if (existingUser.email === undefined) {
        q.reject('Invalid email format');
      }

      q.resolve(true);

      return q.promise;
    }
  }
}]);