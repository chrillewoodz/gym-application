auth.controller('AuthCtrl', ['$scope', '$firebaseAuth', '$location', 'fbRefs', 'validation', 'users', function($scope, $firebaseAuth, $location, fbRefs, validation, users) {
  
  $scope.status = {
    message: ''
  }
  
  // Registration process
    $scope.newUser = {
      email: '',
      password: '',
      confirmedPass: ''
    }

    $scope.register = function() {

      $scope.status = {
        message: ''
      }

      validation.validateSignup($scope.newUser)
        .catch(function(err) {

          // The validation didn't go through,
          // display the error to the user
          $scope.status.message = err;
        })
        .then(function(res) {

          // If validation goes through
          if (res === true) {

            // Create a new user
            users.createNewUser($scope.newUser)
              .then(function() {

                // If successful, authenticate the new user
                // and redirect to home page
                fbRefs.getAuthObj().$authWithPassword({
                  email: $scope.newUser.email,
                  password: $scope.newUser.password
                }).then(function(authData) {
                  $location.path('/home');
                });
              })
              .catch(function(error) {

                // Check for server side errors
                switch (error.code) {

                  case 'EMAIL_TAKEN': 
                    $scope.status.message = 'Email already registered';
                    break;

                  default: 
                    $scope.status.message = 'Connection error - try again later';
                    break;
                }
            });
          }
      });
    }
  // /registration process
  
  // Login process
  $scope.existingUser = {
    email: '',
    password: ''
  }

  $scope.login = function() {

    validation.validateSignin($scope.existingUser)
      .catch(function(err) {
      
        // The validation didn't go through,
        // display the error to the user 
        $scope.status.message = err;
      })
      .then(function(res) {
      
        // Validation passed
        if (res === true) {
          
          // Attempt to authenticate the user
          users.authUser($scope.existingUser)
            .catch(function(error) {

                // Check for server side error
                switch (error.code) {

                  case 'INVALID_USER': 
                    $scope.status.message = 'Invalid email';
                    break;

                  case 'INVALID_EMAIL': 
                    $scope.status.message = 'Invalid email format';
                    break;    

                  case 'INVALID_PASSWORD': 
                    $scope.status.message = 'Invalid password';
                    break;
                }
            })
            .then(function(authData) {
              $location.path('/home');
          });
        }
    });
  }
  
  // Login for Facebook, Twitter and Google
  $scope.thirdPartyLogin = function(party) {
    
    // Make sure we remove the capital 
    // letter from party name
    var party = party.toLowerCase();
    
    $firebaseAuth(fbRefs.getReference()).$authWithOAuthPopup(party)
      .then(function(authData) {
        $location.path('/home');
      }).catch(function(err) {
        $scope.status.message = err;
    });
    
  }
  
  // /login process
  
  // Logout process
  $scope.logout = function() {
    $firebaseAuth(fbRefs.getReference()).$unauth();
  }
  // /logout process
  
}]);