diet.controller('DietCtrl', ['$scope', '$firebaseAuth', 'fbRefs', 'fbCrud', function($scope, $firebaseAuth, fbRefs, fbCrud) {
  
  // Action states, if changing crud
  // functionality for edit and delete
  // add these here if required
  $scope.actions = {
    add: false
  };
  
  // Get unique user id
  var uid = $firebaseAuth(fbRefs.getReference()).$getAuth().uid;

  // Define our path
  var path = uid + '/diet';
  
  // Get all weeks
  var weeks = fbRefs.getSyncedArray(path);
 
  // Create a three way data binding
  // for our programs
  weeks.$watch(function(event) {

    // Fetch the programs every time it's updated
    weeks = fbRefs.getSyncedArray(path);

    // When the data has finished loading
    // update the programs list
    weeks.$loaded()
      .then(function(res) {
        $scope.weeks = res;
      })
      .catch(function(err) {
        $scope.addError = 'There was an error with the request';
    });
  });
  
  
  $scope.addWeek = function(week) {

    /* Returns a formatted date
     * 
     * @input Obj week.startDate, week.endDate
     *
     * @return String 'dd-mm-yyyy'
     */
    function formatDate(obj) {
      return ['day', 'month', 'year'].map(function (prop) {
        return String(obj[prop]);
      }).join('-');
    }

    // Pass a day, month and year 
    // to the formatDate function
    week.startDate = formatDate(week.startDate);
    week.endDate = formatDate(week.endDate);

    fbCrud.push(path, week)
      .then(function(ref) {

        // Get the key for the item
        // that was just inserted
        var key = ref.key();

        // And assign it to the week
        // object for later use
        week.key = key;

        // Clear the last input field
        // you so don't have to erase it manually
        // if you want to add another week right away
        week.number = '';
      })
      .catch(function(err) {
        $scope.addError = 'Unable to create a new week';
    })
  }
  
  // Add weeks to be removed
  var records = [];
  
  // Check if the 'check all' box is checked or not
  $scope.$watch('checkAll', function() {

    if ($scope.checkAll) {

      // Make sure we only run map
      // if there are any programs added
      if ($scope.weeks) {
        $scope.weeks.map(function(item) {
          records.push(item.key);
          item.checked = true;
        });
      }
    }
    else {

      // Make sure we only run map
      // if there are any programs added
      if ($scope.weeks) {
        $scope.weeks.map(function(item, i) {
          records.splice(i, 1);
          item.checked = false;
        });
      }
    }
  });
}]);