core.factory('title', [function() {
  
  return {
    getTitle: function(url) {
      
      var title;
    
      switch(url) {

        case '/': 
          title = 'Welcome to your gym diary';
          break;

        case '/home': 
          title = 'Home';
          break;

        case '/programs': 
          title = 'My programs';
          break;
          
        case '/diet': 
          title = 'Diet';
          break;
          
        default: 
          title = '';
          break;
      }
      
      return title;
    }
  }
}]);