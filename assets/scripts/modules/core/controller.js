core.controller('MainCtrl', ['$scope', '$firebaseAuth', '$location', 'title', 'fbRefs', function($scope, $firebaseAuth, $location, title, fbRefs) {
  
  (function() {
    
    //Keep track of the authentication state
    fbRefs.getAuthObj().$onAuth(function(authData) {
      
      $scope.isAuth = $firebaseAuth(fbRefs.getReference()).$getAuth();
      
      if (!authData) {
        $location.path('/');
      }
    });
  }());

  (function() {
    
    var history = [];

    $scope.$on('$routeChangeSuccess', function() {
      
      var path = $location.$$path;
      
      history.push(path);
      
      // Update site title
      $scope.title = title.getTitle(path);
    });

    // Go back to previous location
    $scope.back = function() {
      
      var prevUrl = history.length > 1 ? history.splice(-2)[0] : '/';
      
      $location.path(prevUrl);
    };
  }());
  
}]);