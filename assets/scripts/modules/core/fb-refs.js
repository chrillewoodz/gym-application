auth.factory('fbRefs', ['$firebaseObject', '$firebaseArray', '$firebaseAuth', function($firebaseObject, $firebaseArray, $firebaseAuth) {

  return {
    getReference: function(path) {
      return path ? new Firebase('https://gym-application.firebaseio.com/' + path) : new Firebase('https://gym-application.firebaseio.com');  
    },
    getSyncedObj: function(path) {
      return $firebaseObject(this.getReference(path));
    },
    getSyncedArray: function(path) {
      return $firebaseArray(this.getReference(path));
    },
    getAuthObj: function(path) {
      return $firebaseAuth(this.getReference(path));
    }
  };
}]);