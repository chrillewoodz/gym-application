core.directive('backButton', [function() {
  
  return {
    restrict: 'E',
    template: '<a ng-click="back()"><button class="btn btn-md btn-default button-back"><span class="glyphicon glyphicon-circle-arrow-left"></span>Back</button></a>'
  }
}]);