core.directive('ngEnter', function () {
  
  return function (scope, element, attrs) {
    
    element.bind('keydown keypress', function (event) {
      
      // If we hit the ENTER key
      // execute some code
      if(event.which === 13) {
        scope.$apply(function (){
          scope.$eval(attrs.ngEnter);
        });

        // Prevent this from executing
        // any default actions on the element
        event.preventDefault();
      }
    });
  };
});