'use strict';

// Main module
//
// Inject sub modules here
var core = angular.module('gymApp', [
  'ngRoute',
  'ngTouch',
  'firebase',
  'auth',
  'crud',
  'navigation',
  'programs',
  'single-program',
  'diet',
  'body'
]);

// Sub modules
//
// Inject these into main module
var nav = angular.module('navigation', []),
    auth = angular.module('auth', []),
    crud = angular.module('crud', []),
    programs = angular.module('programs', []),
    singleProgram = angular.module('single-program', []),
    diet = angular.module('diet', []),
    body = angular.module('body', []);